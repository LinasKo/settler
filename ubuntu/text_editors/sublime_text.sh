#!/bin/bash
set -e

function make_backup {
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cp $1 $script_dir/../backups/$(basename $1)-$(date +"%Y-%m-%d--%H-%M-%S")
  echo "[Settler] Backed up $1"
}

function strip_comments {
  sed '/^\/\//d' -i $1
}


# Sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update
sudo apt install -y sublime-text

# Font
sudo apt install -y fonts-firacode jq moreutils
subl_settings="$HOME/.config/sublime-text-3/Packages/User/Preferences.sublime-settings"
make_backup $subl_settings
strip_comments $subl_settings
jq '.font_face = "Fira Code"' $subl_settings --indent 4 | sponge $subl_settings
jq '.font_options = ["gray_antialias"]' $subl_settings --indent 4 | sponge $subl_settings

echo ""
echo "[Settler] Installed Sublime Text with Fira-Code."
echo "Install dependencies:"
echo "  APT: moreutils jq"
