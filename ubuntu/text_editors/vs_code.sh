#!/bin/bash
set -e

function make_backup {
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cp $1 $script_dir/../backups/$(basename $1)-$(date +"%Y-%m-%d--%H-%M-%S")
  echo "[Settler] Backed up $1"
}

function strip_comments {
  sed '/^\/\//d' -i $1
}


# Snap
sudo apt update
sudo apt install -y snapd

# VS Code
sudo snap install --classic code

# Settings
sudo apt install -y fonts-firacode jq moreutils
code_settings="$HOME/.config/Code/User/settings.json"
if [[ ! -e $code_settings ]]; then
    mkdir -p $(dirname $code_settings) &> /dev/null
    echo "{}" > $code_settings
fi
make_backup $code_settings
strip_comments $code_settings

# General
jq '."editor.mouseWheelZoom" = true' $code_settings --indent 4 | sponge $code_settings

# Font
fonts="'Fira Code', 'Droid Sans Mono', 'monospace', monospace, 'Droid Sans Fallback'"
jq --arg fonts "$fonts" '."editor.fontFamily" = $fonts' $code_settings --indent 4 | sponge $code_settings
jq '."editor.fontLigatures" = true' $code_settings --indent 4 | sponge $code_settings

echo ""
echo "[Settler] Installed VS Code with Fira-Code."
echo "Install dependencies:"
echo "  APT: snapd moreutils jq"
