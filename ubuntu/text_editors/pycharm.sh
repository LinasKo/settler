#!/bin/bash
set -e


# Snap
sudo apt update
sudo apt install -y snapd

# PyCharm
sudo snap install pycharm-community --classic

# Fonts
sudo apt install -y fonts-firacode
echo "[Settler] FiraCode has been installed but you will have to set it manually."
echo "Opening instructions page on your browser in 5s"
sleep 5s
xdg-open https://github.com/tonsky/FiraCode/wiki/Intellij-products-instructions &

echo ""
echo "[Settler] Installed PyCharm with Fira-Code."
echo "Install dependencies:"
echo "  APT: snapd"
