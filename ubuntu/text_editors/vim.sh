#!/bin/bash
set -e


sudo apt install -y vim
sudo update-alternatives --set editor /usr/bin/vim.basic

echo ""
echo "[Settler] Installed Vim."
