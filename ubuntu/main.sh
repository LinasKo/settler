#!/bin/bash
set -e


./install_basics.sh
./languages/python.sh
./install_extras.sh
./text_editors/vim.sh
./text_editors/sublime_text.sh
./text_editors/vs_code.sh
./text_editors/pycharm.sh

echo "Now source the .bashrc and you're good to go!"
