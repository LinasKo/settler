#!/bin/bash
set -e

function make_backup {
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cp $1 $script_dir/backups/$(basename $1)-$(date +"%Y-%m-%d--%H-%M-%S")
  echo "[Settler] Backed up $1"
}

brc="$HOME/.bashrc"
make_backup $brc
function add_to_bashrc {
  echo "$1" >> "$brc"
}


sudo apt update

# Text comparison
sudo apt install -y meld

# Do The Right Extraction
sudo apt install -y dtrx

# Bash failure recovery
sudo apt install -y python3 python3-dev python3-pip python3-setuptools
sudo pip3 install -U thefuck
section="# Fuck"
if ! grep -Fxq "$section" "$brc"; then
  add_to_bashrc ""
  add_to_bashrc "$section"
  add_to_bashrc "eval $(thefuck --alias)"
fi
