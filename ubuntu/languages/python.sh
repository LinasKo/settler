#!/bin/bash
set -e

function make_backup {
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cp $1 $script_dir/../backups/$(basename $1)-$(date +"%Y-%m-%d--%H-%M-%S")
  echo "[Settler] Backed up $1"
}

brc="$HOME/.bashrc"
make_backup $brc
function add_to_bashrc {
  echo "$1" >> "$brc"
}


sudo apt update

sudo apt install -y \
  python python-pip python-dev virtualenv python-testresources \
  python3 python3-pip python3-dev python3-venv python3-testresources

pip install -U wheel setuptools numpy
pip3 install -U wheel setuptools numpy

pip3 install -U ipython notebook

section="# Python Shortcuts"
if ! grep -Fxq "$section" "$brc"; then
  add_to_bashrc ""
  add_to_bashrc "$section"
  add_to_bashrc "alias p3=\"ipython3\""
fi

echo ""
echo "[Settler] Installed Python."
