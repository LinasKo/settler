#!/bin/bash
set -e

function make_backup {
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cp $1 $script_dir/backups/$(basename $1)-$(date +"%Y-%m-%d--%H-%M-%S")
  echo "[Settler] Backed up $1"
}

brc="$HOME/.bashrc"
make_backup $brc
function add_to_bashrc {
  echo "$1" >> "$brc"
}


sudo apt update

# General
sudo apt install -y build-essential 

section="# Useful functions / aliases"
if ! grep -Fxq "$section" "$brc"; then
  add_to_bashrc ""
  add_to_bashrc "$section"
  add_to_bashrc "alias x=\"xdg-open\""
  add_to_bashrc "alias brc=\"vim $HOME/.bashrc\""
  add_to_bashrc "alias sbrc=\". $HOME/.bashrc && echo '. $HOME/.bashrc'\""
  add_to_bashrc "alias update='echo \"Runnung: sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt dist-upgrade && sudo apt autoremove && sudo apt clean\" && \\"
  add_to_bashrc "  sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt dist-upgrade && sudo apt autoremove && sudo apt clean'"
fi

# Git
sudo apt install -y git

section="# Git shortcuts"
if ! grep -Fxq "$section" "$brc"; then
  add_to_bashrc ""
  add_to_bashrc "$section"
  add_to_bashrc "alias st='git status'"
  add_to_bashrc "alias br='git branch'"
  add_to_bashrc "alias cm='git commit'"
  add_to_bashrc "alias cma='git commit --amend'"
  add_to_bashrc "alias ch='git checkout'"
  add_to_bashrc "alias gdiff='git diff'"
  add_to_bashrc "alias gdiffc='git diff --cached'"
fi

# See folder as a tree
sudo apt install -y tree

# Process monitoring
sudo apt install -y htop

# sed for JSON
sudo apt install -y jq

# Small bash tools
sudo apt install -y moreutils
